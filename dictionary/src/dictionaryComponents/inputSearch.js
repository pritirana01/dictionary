import React, { useState } from "react";
import axios from 'axios'
const mainBox = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '1000px',
    height: '500px',
    border: 'none',
    fontSize:'20px',
    borderRadius:'20px',
    background: '#F3D7CA',
    boxShadow:'10px 10px 10px rgba(255, 255, 255, 0.5)'
};
const inputStyle={
    position:'relative',
    width:'400px',
    left:'30px',
    top:'-10px',
    height:'40px',
    fontSize:'20px',
    borderRadius:'20px',
    border:'none',
    overFlow:'hidden',
    outline:'none'
}
const searchStyle={
    position:'relative',
    top:'-10px',
    left:'-44px',
    height:'40px',
    overFlow:'hidden',
    border:'none',
    fontSize:'20px',
    borderRadius:'0px 4px 0px 4px',
    backgroundColor:'black',
    color:'white'
}
const errorMsgStyle={
    position:'relative',
    top:'150px',
    fontSize:'25px'
}
const meaningStyling={
    position:'relative',
    top:'-20px',
    fontSize:'25px',
    display:'inline-block',
    backgroundColor:'#E6A4B4',
    padding:'10px'
   
}
const meaningHeadline={
    position:'relative',
    top:'5px',
    fontSize:'25px',
    display:'inline-block',
    backgroundColor:'#E6A4B4',


    
}
/*const boxWord={
   
   
}*/
const appNameStyle={
    position:'relative',
    top:'-10px',
    fontSize:'40px'
}
const listStyle={
    backgroundColor:'#F3D7CA',
    padding:'10px',
    color:'black',
    margin:'5px',
    listStyle:'none'

}
function WordSearch(){
    const [word, setWord] = useState('')
    const [meanings, setMeanings] = useState([])
    const [error, setError] = useState(null);

    async function getData(){
        try{
            const response = await axios.get(`https://api.dictionaryapi.dev/api/v2/entries/en/${word}`)
            const words = response.data
            
            let fetchedMeanings = [];
            for (let i = 0; i < words.length; i++) {
                const meanings = words[i].meanings;
                for (let j = 0; j < meanings.length && fetchedMeanings.length < 2; j++) {
                    fetchedMeanings.push(meanings[j].definitions[0].definition);
                }
            }
            
            if (fetchedMeanings.length === 0) {
                setError("No meanings found for the word.");
            } else {
                setMeanings(fetchedMeanings);
                setError(null);
            }
        }
        catch(error){
         //   console.log("Internal server error", error);
            setError("There is no data related to this word");
        }
    }

    return (
        <div>
          
            <div style={mainBox} >
            <h1 style={appNameStyle}>Dictionary App</h1>
            <input style={inputStyle}
                type="text" 
                value={word} 
                onChange={(event) => setWord(event.target.value)}
            />
            <button onClick={getData} style={searchStyle}>Search</button>
            <div>
                {error && <p style={errorMsgStyle}>{error}</p>}
                {meanings.length > 0 && (
                    <div>
                      <div > <h2 style={meaningHeadline}>Search For :- {word}</h2></div>  
                        <ul style={meaningStyling}>
                            {meanings.map((meaning, index) => (
                                <li key={index} style={listStyle}>{meaning}</li>
                            ))}
                        </ul>
                    </div>
                )}
            </div>
        </div>
            </div>
          
    )
}

export default WordSearch;
